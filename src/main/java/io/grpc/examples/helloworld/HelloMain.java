package io.grpc.examples.helloworld;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.examples.routeguide.Helloworld;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Optional;

public class HelloMain {
    private static final int DEFAULT_PORT = 50052;
    private static final Logger LOGGER = LoggerFactory.getLogger(HelloWorldService.class);

    public static void main(String[] args) throws IOException, InterruptedException {
        HelloWorldService service = new HelloWorldService();
        Server server = ServerBuilder.forPort(DEFAULT_PORT)
                .addService(service)
                .build()
                .start();

        server.awaitTermination();

        LOGGER.info("Service started, listening at port {}", DEFAULT_PORT);
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            // Use stderr here since the logger may have been reset by its JVM shutdown hook.
            LOGGER.info("*** shutting down gRPC server since JVM is shutting down");
            server.shutdown();
            LOGGER.info("*** server shut down");
        }));


        HelloWorldClient client = new HelloWorldClient("localhost", DEFAULT_PORT);
        try {
            Optional<Helloworld.HelloResponse> response = client.greet("tarek");
            LOGGER.info("Service response {}", response);
        } finally {
            client.shutdown();
        }

    }
}
