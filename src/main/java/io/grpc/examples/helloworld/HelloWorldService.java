package io.grpc.examples.helloworld;

import io.grpc.examples.routeguide.GreeterGrpc;
import io.grpc.examples.routeguide.Helloworld.HelloRequest;
import io.grpc.examples.routeguide.Helloworld.HelloResponse;
import io.grpc.stub.StreamObserver;

public class HelloWorldService extends GreeterGrpc.GreeterImplBase {
    @Override
    public void sayHello(HelloRequest request, StreamObserver<HelloResponse> responseObserver) {
        HelloResponse response = HelloResponse.newBuilder()
                .setMessage("Hello " + request.getName()).build();
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void sayHelloAgain(HelloRequest request, StreamObserver<HelloResponse> responseObserver) {
        super.sayHelloAgain(request, responseObserver);
    }
}
